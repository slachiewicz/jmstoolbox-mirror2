=====================================================================
Basic procedure to setup the development environnement for JMSToolBox
=====================================================================

1.
- Install a jdk v8 64 bits to run eclipse


2.
- Download eclipse Neon RCP v4.6.2+ 64 bits and unzip it somewhere (Neon EE may be a better option as it allows to generate JAXB classes)


3. 
- start eclipse on a new workspace


4.
- Install the "E4 Tools" 
  Update Site: Mars - http://download.eclipse.org/releases/neon
  - General Purpose Tools
    - Eclipse e4 Tools Developper Resources


5.
- Add subclipse plugin. 
  Update Site: https://dl.bintray.com/subclipse/releases/subclipse/latest/
  Check all options


6. (optional)
- Install WindowBuilder (http://www.eclipse.org/windowbuilder/download.php)
  Update Site : Mars - http://download.eclipse.org/releases/neon
  - General Purpose Tools
    - SWT Designer (will pull; WindowsBuilder plugins as dependencies)

	
7. (optionnal)
- Install extra spy tools 
  Update Site: http://download.eclipse.org/e4/snapshots/org.eclipse.e4.tools/latest/	
  - check "Eclipse 4 - All Spies"


8.
Checkout code from trunk: https://svn.code.sf.net/p/jmstoolbox/code
(all folders as projects except project "org.titou10.jtb.hook" is not currently used so no need to checkout)


9. 
- import the following "java code formatter" file into eclipse preferences (preferences/java/code style/formatter/import...): 
  org.titou10.jtb.core/doc/eclipse_java_formatter.xml
- edit preferences/Java/Editor/Save Actions:
  check "format source code" and "organize imports"



10.
- Run/test. Either:
  - open org.titou10.jtb.product/org.titou10.jtb.product, tab "overview", "Launch an eclipse application"
  - create a runtime configuration  and run it

===========================
For headless builds only 
===========================
- Install a jre v8 32bits in some place
- Install a jre v8 64bits in some place 

- Change the following properties in org.titou10.jtb.build/pom.xml to point to the correct locations:
   - jtb.jre8.32
   - jtb.jre8.64  
- right click on pom.xml, "Run As/Maven build"
  - choose "clean verify" as goal
- distibutables will be in org.titou10.jtb.build/dist

    
  
